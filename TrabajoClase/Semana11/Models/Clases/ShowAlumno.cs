﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Semana11.Models.Clases
{
    public class ShowAlumno
    {
        [Key]
        public string carnet { get; set; }
        public string nombre { get; set; }
        public string apellido { get; set; }
    }
}