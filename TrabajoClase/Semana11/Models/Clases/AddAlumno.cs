﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
namespace Semana11.Models.Clases
{
    public class AddAlumno
    {
        [Required(ErrorMessage = "El campo es obligatorio.")]
        [carnetExiste]
        [Key]
        public string carnet { get; set; }
        [Required(ErrorMessage = "El campo es obligatorio.")]
        [MaxLength(30)]
        public string nombre { get; set; }
        [Required(ErrorMessage = "El campo es obligatorio.")]
        [MaxLength(30)]
        public string apellido { get; set; }
    }

    public class carnetExiste : ValidationAttribute
    {
        List<string> list = new List<string>() { "111", "222", "444" };

        public override bool IsValid(object value)
        {
            return !list.Contains(value);
        }
    }
}