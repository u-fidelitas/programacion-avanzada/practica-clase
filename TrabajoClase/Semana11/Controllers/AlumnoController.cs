﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Semana11.Models;
using Semana11.Models.Clases;

namespace Semana11.Controllers
{
    public class AlumnoController : Controller
    {
        // GET: Alumno
        public ActionResult Index()
        {
            return View();
        }

        // POST: Alumno
        public ActionResult Add()
        {
            return View();
        }

        // POST: Alumno
        TrabajoClaseDBConnStr db = new TrabajoClaseDBConnStr();
        [HttpPost]
        public ActionResult Add(AddAlumno model)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            Alumno alumno = new Alumno()
            {
                apellido = model.apellido,
                nombre = model.nombre,
                carnet = model.carnet
            };

            db.Alumno.Add(alumno);
            db.SaveChanges();
            return RedirectToAction("Exitoso", model);
        }

        public ActionResult Exitoso()
        {
            return View();
        }
    }
}